﻿using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using NATS.Client;
using NatsExample.Core;

namespace NatsExample.WebApplication.Controllers;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] _Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly IConnection _connection;

    public WeatherForecastController(IConnection connection)
    {
        _connection = connection ?? throw new ArgumentNullException(nameof(connection));
    }

    [HttpGet]
    public ValueTask Get()
    {
        var weather = new WeatherForecast
        {
            Date = DateOnly.FromDateTime(DateTime.Now),
            TemperatureC = Random.Shared.Next(-20, 55),
            Summary = _Summaries[Random.Shared.Next(_Summaries.Length)]
        };
        _connection.Publish("temperatures", Encoding.UTF8.GetBytes(JsonSerializer.Serialize(weather)));

        return ValueTask.CompletedTask;
    }
}
