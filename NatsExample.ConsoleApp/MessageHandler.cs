﻿using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using NatsExample.Core;

namespace NatsExample.ConsoleApp;

internal class MessageHandler : IMessageHandler
{
    private readonly ILogger<MessageHandler> _logger;

    public MessageHandler(ILogger<MessageHandler> logger)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
    }

    public ValueTask HandleAsync(ReadOnlyMemory<byte> data, CancellationToken cancellationToken = default)
    {
        var json = Encoding.UTF8.GetString(data.Span);

        var weatherForecast = JsonSerializer.Deserialize<WeatherForecast>(json)
            ?? throw new NullReferenceException();

        _logger.LogInformation("WeatherForecast:{weatherForecast}", JsonSerializer.Serialize(weatherForecast));

        return ValueTask.CompletedTask;
    }
}
