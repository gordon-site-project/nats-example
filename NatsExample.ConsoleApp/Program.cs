﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NATS.Client;
using NATS.Client.Rx;
using NATS.Client.Rx.Ops;
using NatsExample.ConsoleApp;
using NatsExample.Core;

var builder = Host.CreateApplicationBuilder(args);

builder.Configuration
    .AddJsonFile("appsettings.json", true)
    .AddEnvironmentVariables();

builder.Services
    .AddSingleton(sp =>
    {
        var natsOptions = ConnectionFactory.GetDefaultOptions();
        natsOptions.Url = builder.Configuration.GetConnectionString("Nats");
        return new ConnectionFactory().CreateConnection(natsOptions);
    });
builder.Services.AddSingleton<IMessageHandler, MessageHandler>();

var app = builder.Build();

var connection = app.Services.GetRequiredService<IConnection>();
var messageHandler = app.Services.GetRequiredService<IMessageHandler>();
var temperatures =
    connection.Observe("temperatures")
        .Where(m => m.Data?.Any() == true)
        .Select(m => m.Data);

temperatures.Subscribe(data => messageHandler.HandleAsync(data));

// 等待使用者輸入以停止程式
Console.WriteLine("Press any key to stop...");
Console.ReadKey();

// 取消訂閱並關閉連線

connection.Close();
