﻿namespace NatsExample.Core;

public interface IMessageHandler
{
    ValueTask HandleAsync(ReadOnlyMemory<byte> data, CancellationToken cancellationToken = default);
}
